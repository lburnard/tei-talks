# TEI-Talks

XML source for talks about TEI, mostly in French

## Description
I have always authored talks and tutorials and similar pedagogic materials in TEI XML, resisting the seductive folderoll of powerpoint, slideshare etc. These folders are cloned from the much bigger collection in my primary github repo at https://github.com/lb42/tei-fr/tree/master/Talks which has a different login, of course.


## Usage
To generate HTML or PDF slides from the talks, I mostly use slidy or latex. For details of the setup, see https://github.com/lb42/TEIslides/

The generated files are not kept here.

## Support
Issues welcomed. 

## Authors and acknowledgment
Much of this material has a long and obscure history, being translated with the aid of many kind-hearted French colleagues from sources I stole from Sebastian, which James then stole from me. Some of it I made up myself. 

## License
Creative Commons, with acknowledgment plz.

## Project status
Will be kept up to date as long as French colleagues keep inviting me.
